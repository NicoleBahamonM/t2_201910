package model.data_structures;

/**
 * Abstract Data Type for a linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface ILinkedList<T> extends Iterable<T> {

	public Integer getSize();

	public void addFirst(T elem);

	public void addAtEnd(T elem);

	public void addAtK(T elem, int pos);

	public T get(int pos);

	public T delete(int pos);

	public T next();

	public T current();

	public T previous();



}




