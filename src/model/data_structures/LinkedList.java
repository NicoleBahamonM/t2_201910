package model.data_structures;
import java.util.Iterator;

public class LinkedList<T> implements ILinkedList<T>{
	// -----------------------------------------------------------------
	// Atributos
	// -----------------------------------------------------------------
	private Nodo Actual;
	private Nodo Primero;
	private Nodo Ultimo;
	private int size;


	public class IteratorLinkedList<T> implements Iterator<T>
	{
		private Nodo<T> actual;

		public IteratorLinkedList(Nodo<T> pActual)
		{
			actual=pActual;
		}

		public boolean hasNext() 
		{
			if (actual!=null)
			{
				return true;
			}
			return false;
		}

		public T next() 
		{
			Nodo<T> referencia = actual;
			T item = referencia.darElement();
			actual = actual.darNext(); 
			return item;
		}

		public void remove()
		{

		}
	}


	public LinkedList()
	{
		Primero=null;
		Ultimo=null;
		Actual=Primero;
	}	

	public Iterator<T> iterator() 
	{			
		return new IteratorLinkedList<T>(Primero);
	}	


	public Integer getSize()
	{
		return size;
	}

	public void addFirst(T elem)
	{
		Nodo<T> aAgregar=new Nodo<T>(elem);
		if(Primero==null)
		{
			Primero=aAgregar;
			Ultimo=Primero;
		}
		else
		{
			aAgregar.modificarNext(Primero);
			aAgregar.modificarPrevious(null);
			Primero=aAgregar;
		}

		size++;
	}

	public void addAtEnd(T elem){
		Nodo<T> aAgregar=new Nodo<T>(elem);
		if(Primero==null)
		{
			Primero=aAgregar;
			Ultimo=Primero;
		}
		else
		{
			aAgregar.modificarNext(null);
			aAgregar.modificarPrevious(Ultimo);
			Ultimo=aAgregar;
		}
		size++;
	}

	public void addAtK(T elem, int pos)
	{
		if(pos==0)
		{
			Nodo<T> aAgregar=new Nodo<T>(elem);
			addFirst(elem);
		}
		else if(pos==size)
		{
			Nodo<T> aAgregar=new Nodo<T>(elem);
			addAtEnd(elem);
		}
		else if(pos>size||pos<0)
		{

		}
		else
		{
			Nodo<T> aAgregar=new Nodo<T>(elem);
			Nodo<T> aNext=new Nodo<T>(get(pos));
			aAgregar.modificarNext(aNext);
			aNext.cambiarElement(elem);

		}
	}



	public T get(int pos)
	{

		Nodo<T> aux = Primero;

		for( int cont = 0; cont < pos; cont++ )
		{
			aux = aux.darNext( );
		}

		return aux.darElement();

	}

	public T delete(int pos)
	{
		Nodo<T> aux=new Nodo<T>(get(pos));
		Nodo<T> ant=aux.darPrevious();
		Nodo<T> sig=aux.darNext();
		ant.modificarNext(sig);
		sig.modificarPrevious(ant);

		return aux.darElement();

	}

	public T next()
	{
		Nodo aux=Actual.darNext();
		return (T) aux.darElement();
	}

	public T current()
	{
		return (T) Actual;
	}

	public T previous()
	{
		Nodo aux=Actual.darPrevious();
		return (T) aux.darElement();
	}

}
