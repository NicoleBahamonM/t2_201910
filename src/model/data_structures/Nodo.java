package model.data_structures;

public class Nodo<T> {


	private Nodo<T> next;
	private Nodo<T> previous;
	private T element;
	private int pos;


	public Nodo(T pElemento){
		element=pElemento;
		next=null;
		previous=null;
	}
	public T darElement(){
		return element;
	}

	public int darPos(){
		return pos;
	}

	public Nodo<T> darNext(){
		return next;
	}
	public Nodo<T> darPrevious(){
		return previous;
	}

	public void modificarNext(Nodo<T> nodo){
		next=nodo;
	}
	public void modificarPrevious(Nodo<T> nodo){
		previous=nodo;

	}

	public void cambiarElement(T pElement){
		element= pElement;
	}


}



